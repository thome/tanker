vm_build_xml_description_hooks()
{
    read -d  -r extra <<EOF
<features>
    <acpi/>
    <apic/>
</features>

EOF
    xml_extra+="$extra"
    xml_devices["network"]="   <interface type='$ln_type'>
            $ln_source
            <mac address='$instancemac'/>
            <target dev='vnet0'/>
            <model type='e1000'/>
            <alias name='net0'/>
        </interface>
    "
}

vm_build_install_hooks()
{
    install_delay=60
}

vm_build_default_parameter_hooks()
{
    case "$chipset" in
        q35)
            inform major "Rewriting chipset choice 'q35', incompatible with freebsd"
            chipset=default
            ;;
    esac
}

vm_build_cloud_init_tree_hooks()
{
    cat > "$ci/freebsd.yaml" <<EOF
# seems that anyway, cloud-init is jailed among rc.local scripts, so we
# can't really hope for something to happen before /etc/rc.d/ssh gets
# called.
#
# We might be willing to run it right now, but even then, that wouldn't
# be a good idea. bootcmd kicks in *before* we write our
# host ssh keys, so it's certainly not the place to start sshd.

bootcmd:
  - echo "sshd_enable=YES" >> /etc/rc.conf
  - echo "sshd_ecdsa_enable=no" >> /etc/rc.conf
  - |
    sed -e 's/#*PermitRootLogin.*/PermitRootLogin without-password/' /etc/ssh/sshd_config > /etc/ssh/sshd_config.mod
    mv /etc/ssh/sshd_config.mod /etc/ssh/sshd_config

runcmd:
  - /etc/rc.d/sshd start
  # note that freebsd has clang by default, not gcc (of course)
  # - env ASSUME_ALWAYS_YES=yes pkg install bash perl5 vim-console git cmake gmake python3 py37-sqlite3 gmp hwloc
  # - perl -ne 'print unless /^cloudinit_enable/;' -i /etc/rc.conf
  - (sed -e '/^hostname/ d' ;  echo "hostname=\"$shortid\"") < /etc/rc.conf | sort -u > /etc/rc.conf.new && mv /etc/rc.conf.new /etc/rc.conf
  # - /usr/local/bin/cloud-init clean
  - rm -f /var/db/dhclient.leases.em0
  # - echo "interface \"em0\" { send host-name \"$shortid\"; }" > /etc/dhclient.conf

# the dhclient tweak above works well for the _next_ boot.


# growpart is just plain buggy on freebsd.
# It wants the partition to be something like vtbd0p1, while we have
# things such as /dev/vtbd0s1a or /dev/ada0s1a, depending on whether we
# have a virtio disk or not.
# Better disable it, or make really sure that you have a virtio device.
growpart:
  mode: off
resize_rootfs: false
EOF

}



disabled_vm_build_cloud_init_make_xml_bit_hook() {
    cat <<EOF
    <disk type='file' device='cdrom'>
            <driver name='qemu' type='raw'/>
            <source file='$1'/>
            <target dev='sda' bus='usb'/>
            <readonly/>
        </disk>
EOF
}

vm_build_create_intermediary_image_hook()
{
    if ! [[ "$url" =~ ^freebsd-cloud-image:///([^\?]*)(\?(.*))?$ ]] ; then return; fi
    release="${BASH_REMATCH[1]}"
    extra_variables=()
    if [ "${BASH_REMATCH[3]}" ] ; then
        query="&${BASH_REMATCH[3]}"
        while [[ "$query" =~ ^\&([^\&]*)(.*)$ ]] ; do
            query="${BASH_REMATCH[2]}"
            parameter="${BASH_REMATCH[1]}"
            if [[ "$parameter" =~ ^([a-zA-Z_][a-zA-Z0-9_]*)=([a-zA-Z0-9_,:-]*)$ ]] ; then
                # eval "$parameter"
                # eval "export ${BASH_REMATCH[1]}"
                extra_variables+=(-"${BASH_REMATCH[1]}" "${BASH_REMATCH[2]}")
            else
                inform error "parameter $parameter is ignored"
            fi
        done
    fi
    script="$mydir/freebsd-cloud-image/freebsd-cloud-image.sh"
    if ! [ -x "$script" ] ; then
        inform failure "No script $script"
    fi
    script_args=(
        -q
        -Z $instancedisksize
        -C "$TANKER_DATABASE/sources"
        -o "$TANKER_DATABASE/sources/$IMAGENAME_AND_TAG.qcow2"
        -n em0
        -R $release)
    "$script" "${script_args[@]}" "${extra_variables[@]}"
    sourcedisk="$TANKER_DATABASE/sources/${IMAGENAME_AND_TAG}.qcow2"
    sourcedisk="$(realpath $sourcedisk)"
    inform debug "sourcedisk is now $sourcedisk"
}

