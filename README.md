
tanker, a docker-like wrapper around libvirt
============================================

The primary goal of this script is to assist in provisioning FreeBSD
virtual machines, mostly for continuous integration purposes. We use
qemu-img backing stores to allow relatively lightweight layers, but the
process remains heavy nevertheless.


Requirements
------------

The main testing platform is Debian GNU/Linux, most recent version. Other
Linux distributions might work, with reasonably good chance if they are
up to date, but your mileage may vary widely.

This script requires a fairly typical set of unix tools. The most
important requirement is that the user running the script must have
access to `libvirt` **and** `docker` (this typically means that the user
must be a member of the relevant unix groups, including possibly the
`kvm` group).

Beyond that, ssh is used a lot, and probably several other mundane tools.


Installation
------------

This repository uses a submodule. When checking out, make sure that you do:
```
git submodule update --init
```

A database of known images must be created automatically before the
script can be used. To do so, just type `make`.

Usage
-----

Several operations are defined:
 - [preparing a handy virtual machine image](#preparing-a-handy-virtual-machine-image)
 - [building a virtual machine image](#building-a-virtual-machine-image)
 - [running a temporary virtual machine from an image](#running-a-temporary-virtual-machine-from-an-image)
 - [connecting to an existing virtual machine](#connecting-to-an-existing-virtual-machine)


Preparing a handy virtual machine image
---------------------------------------

By "handy" here, we mean a virtual machine that easily understands
provisioning information that we give via
[cloud-init](https://cloudinit.readthedocs.io/). This step in particular
is offloaded to a separate script called
[freebsd-cloud-image](https://gitlab.inria.fr/thome/freebsd-cloud-image),
which is a submodule of the present repository.


Building a virtual machine image
--------------------------------

The basic command is `./tanker.sh vm build`. Command-line help for this
particular sub-command can be obtained with:
```
$ ./tanker.sh help vm build
```
The most important argument is the image name, and possibly a tag name.
On the very first run for a given version, the script will spend time
(approximately two minutes) to do the previous step of preparing a handy
virtual machine.

The building process is here to cover the case where you have packages
that you want to have every single time you will be using this particular
FreeBSD version. This depends on your needs, and of course the time to
prepare the image varies accordingly.

The installation process for this additional software is specified with
the common [syntax for pushing commands](#syntax-for-pushing-commands).
For example, if you want bash, gcc, git, cmake, and GNU make, you may
want to build a FreeBSD using this command line.

```
./tanker.sh vm build freebsd:12.2 @guest env ASSUME_ALWAYS_YES=yes pkg install gcc cmake bash git gmake
```

By default, `./tanker.sh vm build` assigns a randomly selected name to
the created machine. It is possible to assign any name that you prefer,
using the `--tag` argument.

Note that the building process is transitive: you can build a new virtual
machine on top of one that you have already built.


Running a temporary virtual machine from an image
-------------------------------------------------

The basic command is `./tanker.sh vm run`. Command-line help for this
particular sub-command can be obtained with:
```
$ ./tanker.sh help vm run
```
An image, whether it is a "pristine" image or one that was already
prepared with `./tanker.sh vm build`, can be derived to a one-shot virtual
machine on which a command can be run. To do so, the `./tanker.sh vm run`
command can be used. Here are a few examples.

 - `./tanker.sh vm run --tty --rm freebsd:12.2`: allocate a tty
   (`--tty`), run a shell, and remove the virtual machine when the shell
   exits

 - `./tanker.sh vm run --tty --rm my-freebsd-12.2 /usr/local/bin/bash`
   start from a machine of our construction that has been built
   previously with `./tanker.sh vm build`, allocate a tty (`--tty`), run
   a specific command (here, bash), and remove the virtual machine when
   the shell exits

As in the case of `./tanker.sh vm build`, the common [syntax for pushing
commands](#syntax-for-pushing-commands) can be used.


Connecting to an existing virtual machine
-----------------------------------------

The basic command is `./tanker.sh vm ssh`. Command-line help for this
particular sub-command can be obtained with:
```
$ ./tanker.sh help vm ssh
```
This is quite similar to `./tanker.sh vm run` with the important
difference that this does not create a new virtual machine. Therefore,
this method can be used to inspect an image. However, you should be fully
aware of the implications of tinkering with the backing store of a qcow2
disk image if you dare to do that on an image that is used within the
backing chain of an existing iamge.

Other recognized sub-commands
-----------------------------

Several other sub-commands are recognized, such as `./tanker.sh vm kill`
or `./tanker.sh pull`, but they're not as useful as the ones above.


Syntax for pushing commands
---------------------------

When telling the script that some command must be run on the guest, we
distinguish between two types of commands:
 - guest commands are run natively on the guest.
 - host commands are run based on material that is present on the host,
   and must be pushed to the guest before it can be executed.

Commands such as `./tanker.sh vm build` or `./tanker.sh vm run`
understand several possible commands on a given command line, separated
by `--`. Each command is thus a sequence of tokens.  The first few tokens
in the sequence determine how the command is executed. Tokens are
understood in that order:
 - optional host/guest token
 - optional username token
 - optional context token
 - script and arguments.

The host/guest tokens are `@host` and `@guest`. When the `@host` token
is present, material will be copied from the host to the guest, and
executed there.

The username token is any token that matches `^([^@]+)@$` (anything that
ends with an `@`). It determines the user under which the command will be
run.

The context token makes sense only when the `@host` token is present.
It determines what material should be copied on the guest.

 - If the context token is a path to a file ending in `.sh`, this file is
   (copied to and) executed on the guest with the provided arguments.
   Note that the name of the script itself is changed to a
   randomly-generated name. In this case, the context is really the
   script itself, and the rest of the command line only gives its
   arguments.

 - If the context token is a path to a file ending in `.tar.gz`, this
   archive is decompressed on the guest in a temporary directory. The
   script whose name is provided after the tar file (defaulting to
   `./script.sh` if there is none) is executed from that directory, with
   the rest of the arguments.

 - If the context token is a path to a directory, the directory
   contents are copied to a randomly-generated directory under /tmp on
   the guest. The script whose name is provided after the directory
   name (defaulting to `script.sh` if there is none) is executed, with
   the rest of the arguments.


When there is no host/guest token at the beginning, the following rule
is used:
 - An absolute path means that `@guest` is implicit
 - A relative path means that `@host` is implicit


Note that for guest commands, the shell environment is **NOT** propagated
to the client. You must arrange so that the material you have in your
environment is passed in another way. For example, this sequence may be
used to propagate all shell settings that are present in the bash array
called `exports`:
```
       @host user@ archive.tar.gz env "${exports[@]}" make
```

Using for other systems
-----------------------

In principle, the mechanism used here should be rather portable to other
systems, provided cloud-init-ready images are available. Alas, there are
many system-specific little things that get in the way. In particular,
while some earlier versions of this script were capable to run on
centos/debian/ubuntu images, this does not seem to work currently. (see
the [`Makefile`](Makefile) for pointers to some presumably cloud-ready
images, but that is only a start.)

No effort is being spent on this at this time, given that docker fits the
purpose much better than this script would anyway. This being said,
patches are welcome.
