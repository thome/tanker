#!/bin/bash

# tanker is meant to work a little bit like docker, but with libvirt
# underneath. The goal is to do things that docker can't do, like run OSes that
# are totally different from the host OS.

me=`realpath "$0"`
mydir=`dirname "$me"`

CONFIG_DIR="$HOME/.tanker"

# We'd like to have the possibility of using usermode networking, but
# unfortunately ssh'ing to the guest is tricky, to the point that I don't
# really feel like going that way. (libvirt inconsistency is one thing,
# but we'd have to find an available port to bind to as well, so in the
# end it would be a pain.
# https://serverfault.com/questions/890520/host-port-forward-with-qemu-through-libvirt-in-user-mode-networking

LIBVIRT_HOST=qemu:///system
LIBVIRT_NETWORK_TYPE=network:default
TANKER_DATABASE="/tmp/tanker-`id -n -u`"

loglevel_error=0
loglevel_warn=1
loglevel_major=2
loglevel_normal=3
loglevel_info=4
loglevel_debug=5
loglevel=info

# scriptdir=`dirname "$me"`/dynamic_vm
# archive_dir=/localdisk/virt/iso
# instances_dir=/localdisk/virt/img
# script_steps="install,bootstrap,compile"

set -e

#{{{ color codes
CSI_BLACK="\e[01;30m"
CSI_RED="\e[01;31m"
CSI_GREEN="\e[01;32m"
CSI_YELLOW="\e[01;33m"
CSI_BLUE="\e[01;34m"
CSI_MAGENTA="\e[01;35m"
CSI_CYAN="\e[01;36m"
CSI_WHITE="\e[01;37m"
CSI_RESET="\e[00;39m\e[m"
CSI_KILLLINE="\e[0K"

# r g b in [0..5]
csi256_rgb() { echo -ne "\e[01;38;5;$((16+$3+6*($2+6*$1)))"m ; }

# r g b in [0..255]
csitrue_rgb() { echo -ne "\e[01;38;2;$1;$2;$3"m ; }
#}}}
script_error()#{{{
{
    echo -e "${CSI_RED}$*${CSI_RESET}"
    exit 1
}
#}}}
inform() #{{{ printing
{
    channel="$1"
    failure=
    if [ "$1" = "failure" ] ; then channel="error" ; failure=1; fi
    shift
    msg="$*"
    case "$channel" in
        error) msg="${CSI_RED}$msg${CSI_RESET}";;
        warn|warning) msg="${CSI_YELLOW}$msg${CSI_RESET}";;
        major) msg="${CSI_BLUE}$msg${CSI_RESET}";;
        normal) ;;
        info) ;;
        debug) ;;
        *) script_error "bad output channel: $channel";;
    esac
    lc="loglevel_${channel}"
    ll="loglevel_${loglevel}"
    if [ "${!ll}" -lt "${!lc}" ] ; then
        return
    fi
    echo -e ":: $msg"
    if [ "$failure" ] ; then exit 1 ; fi
}
#}}}

# {{{ some plumbing for command line parsing.

script_context=
script_context_stack=()
asking_for_help=

expand_associative_array()
{
    # This _copies_ the contents of the second argument (an associative
    # array) to an existing allocated associative array whose name is the
    # first argument.
    a="$1"
    # echo "Appending contents of ass-array $2 to ass-array $1"
    declare -n b="$2"
    for k in "${!b[@]}" ; do
        item="${b[$k]}"
        # echo "${a}[$k]=\"${item}\""
        eval "${a}[$k]=\"${item}\""
    done
}

copy_associative_array()
{
    # This _copies_ the contents of the second argument (an associative
    # array) to a newly allocated associative array whose name is the
    # first argument.
    unset -n "$1"
    unset "$1"
    declare -A "$1"
    expand_associative_array "$@"
}

join_arr() { local IFS="$1" ; shift ; echo "$*" ; }

help_command()
{
    # need to reconstruct the previous context to get the quick
    # description
    # inform debug "script context is ${script_context}"
    # inform debug "script context_stack is ${script_context_stack[*]}"
    if [ "$script_context" ] ; then
        i=0
        k=$((${#script_context_stack[@]}-1))
        t=${script_context_stack[$k]}
        while [ $i -lt $k ]  ; do
            prevcontext+="${script_context_stack[$i]}_"
            let i+=1
        done
        declare -n quickdesc="${prevcontext}commands[$t]"
        # inform debug "${prevcontext}commands[$t]"
        # inform debug "${network_commands[*]}"
        # inform debug "$quickdesc"
    fi
    declare -n var_desc="${script_context}description"
    if ! [ "$var_desc" ] ; then var_desc="$quickdesc" ; fi
    declare -n var_desc_args="${script_context}description_arguments"
    declare -n var_subcommands="${script_context}commands"
    declare -n var_options="${script_context}options"
    if [ "${#var_subcommands[@]}" -gt 0 ] ; then has_subcommands=1 ; fi
    if [ "${#var_suboptions[@]}" -gt 0 ] ; then has_suboptions=1 ; fi
    # options exists because of the caller
    if [ "${#options[@]}" -gt 0 ] ; then has_options=1 ; fi
    usage_string="Usage: $0"
    for c in "${script_context_stack[@]}" ; do usage_string+=" $c" ; done
    if [ "$has_subcommands" ] ; then usage_string+=" COMMAND" ; fi
    if [ "$has_options" ] ; then usage_string+=" [OPTIONS]" ; fi
    if [ "$var_desc_args" ] ; then usage_string+=" $var_desc_args" ; fi
    cat <<EOF
$var_desc

$usage_string
EOF
    if [ "$has_subcommands" ] ; then
        mcmds=()
        cmds=()
        for a in "${!var_subcommands[@]}" ; do
            b="${var_subcommands[$a]}"
            if [[ "${b}" =~ ^Manage ]] ; then
                mcmds+=("$a")
            else
                cmds+=("$a")
            fi
        done
        if [ "${#mcmds[@]}" -gt 0 ] ; then
            echo
            echo "Management Commands:"
            for a in $(sort <<<"${mcmds[@]}") ; do
                echo "  $(printf %-12s $a)${var_subcommands[$a]}"
            done
        fi
        if [ "${#cmds[@]}" -gt 0 ] ; then
            echo
            echo "Commands:"
            for a in $(sort <<<"${cmds[@]}") ; do
                echo "  $(printf %-12s $a)${var_subcommands[$a]}"
            done
        fi
    fi
    printoo() {
        local oo
        declare -n oo=$op
        if [ "${#oo[@]}" -gt 0 ] ; then
            echo "Options (from $1 context):"
            for o in $(sort <<<"${!oo[@]}") ; do
                IFS=, read -d '' -r -a variants <<< "${o%:*}"
                v0="${variants[0]}"
                ww=()
                for v in "${variants[@]}" ; do
                    if [ "${#v}" = 1 ] ; then w="-$v" ; else w="--$v" ; fi
                    ww+=("$w")
                done
                a=$(join_arr , "${ww[@]}")
                if [ "$o" != "${o%:*}" ] ; then
                    opt_type="${o#*:}"
                    a+=" <$opt_type>"
                fi
                ot="${oo[$o]}"
                if [[ "${oo[$o]}" =~ ^\(([^\)]*)\)[[:space:]]*(.*)$ ]] ; then
                    ot="${BASH_REMATCH[2]} (default: ${BASH_REMATCH[1]})"
                fi
                echo "  $(printf %-22s "$a")$ot"
            done
            echo
        fi
    }
    op=options
    echo
    printoo main
    cc=""
    cc_=""
    for c in "${script_context_stack[@]}" ; do
        cc_+="${c}_"
        cc+=" $c"
        op="${cc_}options"
        printoo "\"${cc# }\""
    done
}

prefix_from_stack()
{
    x=
    for y in "$@" ; do x+="${y}_" ; done
    echo "$x"
}

prefixes_from_stack()
{
    x=
    results=("")
    for y in "$@" ; do x+="${y}_" ; results+=("$x"); done
}

process_command_line()
{
    local options_values
    local cc
    local oo
    declare -A "${script_context}options_values"
    # try to recognize a command
    declare -n cc="${script_context}commands"
    declare -A oo
    declare -n ov="${script_context}options_values"
    prefixes_from_stack "${script_context_stack[@]}"
    for c in "${results[@]}" ; do
        expand_associative_array oo "${c}options"
    done
    message_context=""
    if [ "${#script_context_stack[@]}" -gt 0 ] ; then
        message_context="${script_context_stack[@]}: "
    fi
    local fail
    fail() { help_command ; inform failure "${message_context}$*" ; }
    # fill defaults if we have any
    for o in "${!oo[@]}" ; do
        IFS=, read -d '' -r -a variants <<< "${o%:*}"
        v0="${variants[0]}"
        if [[ "${oo[$o]}" =~ ^\(([^\)]*)\)[[:space:]]*(.*)$ ]] ; then
            ov+=(["$v0"]="${BASH_REMATCH[1]}")
        fi
    done
    while [ $# -gt 0 ] ; do
        # inform debug "command line is $*"
        if ([ "$1" = "-h" ] || [ "$1" = "--help" ] || [ "$1" = help ]) && ! [ "$asking_for_help" ] ; then
            asking_for_help=1
            shift
            continue
        fi
        if [ "$1" = -- ] ; then shift; break; fi
        if [[ "$1" =~ ^- ]] ; then
            for o in "${!oo[@]}" ; do
                IFS=, read -d '' -r -a variants <<< "${o%:*}"
                opt_type=
                if [ "$o" != "${o%:*}" ] ; then opt_type="${o#*:}" ; fi
                v0="${variants[0]}"
                for v in "${variants[@]}" ; do
                    if [ "${#v}" = 1 ] ; then w="-$v" ; else w="--$v" ; fi
                    # inform debug "checking if \$1=$1 is $w"
                    if [ "$1" != "$w" ] ; then continue ; fi
                    shift
                    val=1
                    if [ "$opt_type" ] ; then
                        if [ $# -le 1 ] ; then
                            fail "$w requires an argument ($opt_type)"
                            exit 1
                        else
                            val="$1"
                            shift
                        fi
                    fi
                    ov+=(["$v0"]="$val")
                    so="setoption_${script_context}${v0}"
                    if [ "$(type -t $so)" = function ] ; then
                        inform debug "calling $so $val"
                        "$so" "$val"
                    fi
                    continue 3
                done
            done
            fail "unrecognized option $1"
        else
            if [ "${#cc[@]}" -eq 0 ] ; then
                # if there are no subcommands, then we must have a leaf
                # node in the command tree. No point in checking for
                # arguments. And actually it's an equivalence.
                break
            fi
            for c in "${!cc[@]}" ; do
                # inform debug "checking if \$1=$1 is $c"
                if [ "$1" = "$c" ] ; then
                    shift
                    script_context+="${c}_"
                    script_context_stack+=("$c")

                    # go down the tree.
                    # inform debug "recursing ${script_context_stack[@]} + $*"
                    process_command_line "$@"
                fi
            done
            fail "Unexpected command: $*"
        fi
    done
    if [ "$asking_for_help" ] ; then
        help_command
        exit 0
    fi
    sco="${script_context}command"
    # inform debug "checking for function $sco"
    if [ "$(type -t $sco)" = function ] ; then            
        # inform debug "calling $sco $@"
        tools_check
        libvirt_check
        "$sco" "$@"
        exit $?
    elif [ "${#cc[@]}" -gt 0 ] ; then
        fail "please provide a command"
    else
        fail "unimplemented!"
    fi
}

# }}}

v() {
    if [ "${LIBVIRT_HOST}" ] ; then
        virsh -q -c "${LIBVIRT_HOST}" "$@"
    else
        virsh -q "$@"
    fi
}

tools_check() { #{{{
    mandatory_tools=(
        genisoimage
        virsh
        scp
        ssh
        setfacl
        curl
        qemu-img
        ssh-keygen
        ping
        uuidgen
        sha256sum
        perl    # barely needed, in fact
        "$@"
    )
    for tool in "${mandatory_tools[@]}" ; do
        if ! type -p "$tool" > /dev/null 2>&1 ; then
            inform failure "Mandatory tool missing: $tool"
        fi
    done
}
#}}}

libvirt_check() { #{{{
    inform normal "Checking libvirt access"
    v net-list --all > /dev/null
    inform normal "Making sure libvirt's default network is active"
    if ! v net-info default | grep -q "^Active:.*yes" ; then
        v net-start default
    fi
    if ! v net-info default | grep -q "^Active:.*yes" ; then
        inform error "Failed to bring libvirt's default network online"
    fi
}
#}}}

# This is for the names that go in /images
sanitize_id() { #{{{
    x="$1"
    x="${x//:/-}"
    x="${x//\?/-}"
    # the short id easily ends up in paths, and if we ever use env to
    # prefix a call to a script that is within a directory which has an =
    # in it, then the script name will be misinterpreted as an
    # assignment.
    x="${x//=/-}"
    echo "$x"
} #}}}

###########################################################################

#{{{ toplevel verbs
declare -A commands
commands+=(["help"]="Give help")

declare -A options
# we have no active config mechanism for the moment.
# options+=("config:string"	"Location of client config files (default $CONFIG_DIR)")

# disable this option, at least for now.
## options+=("context,c:string"	"Path to the libvirt host (default ${LIBVIRT_HOST:-none})")

options+=(["debug,D"]="Enable debug mode. Equivalent to --loglevel debug")
options+=(["version,v"]="Print version information and quit")
options+=(["database,B:string"]="Database path on filesystem (default $TANKER_DATABASE)")
options+=(["loglevel:string"]="Set the loglevel among error|warn|major|normal|info|debug")
read -d'' -r description <<'EOF'
Tanker is meant to work a little bit like docker, but with libvirt underneath.
The goal is to do things that docker can't do, like run OSes that are totally
different from the host OS.

EOF
setoption_config() { CONFIG_DIR="$1" ; }
setoption_context() {
    LIBVIRT_HOST="$1"
    if [[ $LIBVIRT_HOST =~ ^([^,]*),(user|network:.*)$ ]] ; then
        LIBVIRT_HOST="${BASH_REMATCH[1]}"
        LIBVIRT_NETWORK_TYPE="${BASH_REMATCH[2]}"
    elif [[ $LIBVIRT_HOST =~ system$ ]] ; then
        LIBVIRT_NETWORK_TYPE=network:default
    fi
}
setoption_database() { TANKER_DATABASE="$1" ; }
setoption_loglevel() {
    case "$1" in
        error|warn|major|normal|info|debug) loglevel="$1";;
        *) inform failure "Bad log level: $channel";;
    esac
}
setoption_debug() { loglevel=debug; }
#}}}

# database_mkdirs -- make the different directories in the database if they don't exist yet.{{{
database_mkdirs()
{
    if ! [ -d "$TANKER_DATABASE" ] ; then
        if [[ $TANKER_DATABASE =~ ^/tmp ]] ; then
            inform info "Creating directory $TANKER_DATABASE"
            mkdir -p "$TANKER_DATABASE"
            # libvirt wants the directory with the iso file to be accessible to
            # libvirt-qemu
            inform info "Making $TANKER_DATABASE readable by libvirt-qemu"
            setfacl -m u:libvirt-qemu:rx "$TANKER_DATABASE"
        else
            echo "ERROR: $TANKER_DATABASE doesn't exist" >&2
            exit
        fi
    fi
    for d in images sources ; do
        if ! [ -d "$TANKER_DATABASE/$d" ] ; then
            inform info "Creating directory $TANKER_DATABASE/$d"
            mkdir "$TANKER_DATABASE/$d"
            inform info "Making $TANKER_DATABASE/$d readable by libvirt-qemu"
            setfacl -m u:libvirt-qemu:rx "$TANKER_DATABASE/$d"
        fi
    done
}
#}}}

#{{{ network verbs
commands+=([network]="Manage networks")
declare -A network_commands
network_commands+=([list]="List networks")
network_list_command() { v net-list ; }
#}}}


commands+=([image]="Manage images") #{{{
declare -A image_commands
image_commands+=([list]="List images")
image_list_command() {
    database_mkdirs
    (cd "$TANKER_DATABASE"; find {images,sources} -mindepth 1 -maxdepth 1 -a -type d | xargs -r du -sh)
}
#}}}

commands+=([images]="List images") #{{{
images_command() { image_list_command "$@"; }
#}}}

imagename_extract_tag()
{
    IMAGE_ARGS=()
    case "$1" in
        *:*) IMAGENAME="${1%:*}"; TAG="${1##*:}" ;;
        *) IMAGENAME="$1"; TAG= ;;
    esac
    if [[ "$TAG" =~ ^([^\?]*)(\?(.*))?$ ]] ; then
        TAG="${BASH_REMATCH[1]}"
        if [ "${BASH_REMATCH[3]}" ] ; then
            query="&${BASH_REMATCH[3]}"
            while [[ "$query" =~ ^\&([^\&]*)(.*)$ ]] ; do
                query="${BASH_REMATCH[2]}"
                parameter="${BASH_REMATCH[1]}"
                IMAGE_ARGS+=("$parameter")
            done
        fi
    fi
}
search_backend() #{{{
{
    results=()
    imagename_extract_tag "$1"    # this sets IMAGENAME and TAG (or TAG empty)
    if [ $# -gt 1 ] ; then
        shift
        inform failure "Extraneous arguments $*"
    fi
    desired_arch=x86_64
    desired_format=qcow2
    for parameter in "${IMAGE_ARGS[@]}" ; do
        if [[ "$parameter" =~ ^arch=(.*)$ ]] ; then
            desired_arch="${BASH_REMATCH[1]}"
        elif [[ "$parameter" =~ ^format=(.*)$ ]] ; then
            desired_format="${BASH_REMATCH[1]}"
        else
            inform error "image argument $parameter is ignored"
        fi
    done
    while read x ; do
        read name tag arch format url <<<"$x"
        if [ "$arch" != "$desired_arch" ] ; then continue; fi
        if [ "$format" != "$desired_format" ] ; then continue; fi
        if [ "$IMAGENAME" = "$name" ] && (! [ "$TAG" ] || [ "$TAG" = "$tag" ] ); then
            results+=("$x")
        fi
    done < "$mydir/images.txt"
}
#}}}

commands+=([search]="Search for images") #{{{
pull_description_arguments="IMAGENAME[:TAG]"
read -d'' -r search_description <<'EOF'
This command searches for images and versions. It is more useful (to our
eyes) than its counterpart "docker search", since it uses the same kind
of image names (and tags) than the pull command uses

EOF
# note that "docker search" can't search for tags itself, it's a bit
# weird. For docker, a more useful listing functionality is here:
# https://forums.docker.com/t/how-can-i-list-tags-for-a-repository/32577/8
search_command()
{
    search_backend "$@"
    for a in "${results[@]}" ; do
        read name tag arch format url <<<"$a"
        echo "$name:$tag"
        # echo $a
    done
}
#}}}

commands+=([pull]="Pull an image") #{{{
pull_description_arguments="IMAGENAME"
pull_command()
{
    # This command sets the $pulled_image variable to the path of the
    # image that we just pulled.
    database_mkdirs
    image_parents=()
    url=

    sane1=$(sanitize_id "$1")
    if [ -d "$TANKER_DATABASE/images/$sane1" ] ; then
        if [ -f "$TANKER_DATABASE/images/$sane1/cloudinit_ready" ] ; then
            inform normal "$1 already exists as a VM image, trying to piggy-back on it"
            url="$TANKER_DATABASE/images/$sane1/disk.qcow2"
            image_parents+=($(cat $TANKER_DATABASE/images/$sane1/image-parents))
            inform normal "$1 has parents: ${image_parents[@]}"
        else
            inform failure "$1 already exists as a VM image but cannot be (re-)provisioned with cloud-init"
        fi
    fi
    image_parents+=("$1")

    if ! [ "$instance" ] ; then script_error '$instance must be set up' ; fi
    mkdir -p "$instance"
    echo "${image_parents[@]}" > "$instance/image-parents"

    # this happens if we piggy-back on a previous vm
    if [ "$url" ] ; then
        pulled_image="$url"
        return
    fi

    search_backend "$@"

    if [ "${#results[@]}" = 0 ] ; then
        inform failure "No images found to match $1"
    elif [ "${#results[@]}" -gt 1 ] ; then
        inform error "Image name $1 matches several images:"
        for a in "${results[@]}" ; do
            read name tag arch format url <<<"$a"
            echo "$name:$tag"
            # echo $a
        done
        exit 1
    fi

    read name tag arch format url <<<"${results[0]}"

    case "$url" in
        http://*|https://*|ftp://*)
            (cd $TANKER_DATABASE/sources ; curl -O -C- "$url")
            pulled_image="TANKER_DATABASE/sources/$(basename "$url")"
            ;;
        *)
            inform normal "Skipping download, assuming script takes care of it later for $url"
            ;;
    esac
}
#}}}

    # note for freebsd: avoid q35 ?
    # https://bugs.freebsd.org/bugzilla/show_bug.cgi?id=236922


     # extra images
     #
     # freebsd 11.2 11.3 12.0 12.1
     # https://download.freebsd.org/ftp/releases/VM-IMAGES/12.1-RELEASE/amd64/Latest/FreeBSD-12.1-RELEASE-amd64.qcow2.xz
     #
     # https://wiki.qemu.org/Hosts/BSD seems to be a no-go for openbsd and
     # netbsd

#     read _ instancemac instanceip < <(grep "^$shortid\\b" $scriptdir/machines.list)
#
#     for a in image_url instancemac instanceip ; do
#         if ! [ "${!a}" ] ; then echo "no $a" >&2 ; exit 1 ; fi
#         echo ":: $a ${!a}"
#     done
#     inform major "Fetching $image_url"
#     # no output redirect for this one
#     (cd $archive_dir ; wget -c "$image_url")

commands+=([vm]="Manage vms")
declare -A vm_commands

# these options are common to vm build and vm run
declare -A vm_build_or_run_options
vm_build_or_run_options+=(["memory,m:amount"]="(4G) Allocate this amount of memory to the vm")
vm_build_or_run_options+=(["disksize,Z:amount"]="(8G) Disk size")
vm_build_or_run_options+=(["cpus,n:number"]="(2) Allocate this number of cpus to the vm")
vm_build_or_run_options+=(["chipset:string"]="(q35) Use this chipset")
vm_build_or_run_options+=(["hooks:string"]="Use this script for hooks")
vm_build_or_run_options+=(["no-hooks"]="Disable hooks")
vm_build_or_run_options+=(["replace,R"]="Replace existing VMs")


create_instance_disk() { #{{{
    # arguments:
    #   - url of the source image. image must be present in
    #     $TANKER_DATABASE/sources. The uncompressed image will be put in
    #     the same directory if it does not exist.
    #   - path to the instance disk to create.
    url="$1"
    instancedisk="$2"

    if [[ $url =~ ^/ ]] ; then
        sourcedisk="$url"
    else
        sourcedisk="$TANKER_DATABASE/sources/$(basename $url)"
    fi

    # This **MAY** play weird games with $sourcedisk, including actually
    # cheating with the url, and eventually modifying it to be a
    # different path
    vm_build_create_intermediary_image_hook

    # make sure that $sourcedisk is the path to an existing, decompressed
    # qcow2 image
    if ! [ -f "$sourcedisk" ] ; then
        inform failure "Missing file $sourcedisk"
    fi

    case "$sourcedisk" in
        *.qcow2) : ;;
        *.qcow2.xz)
            tmp="${sourcedisk%.xz}"
            if ! [ -f "$tmp" ] || [ "$sourcedisk" -nt "$tmp" ] ; then
                inform major "Decompressing $(basename $sourcedisk) to $(basename $tmp)"
                tools_check xzcat
                xzcat "$sourcedisk" > "$tmp"
            fi
            sourcedisk="$tmp"
            ;;
        *)  if [[ `file "$sourcedisk"` =~ QCOW2 ]] ; then : ; else
                inform failure "Unrecognized image file format $sourcedisk"
            fi
            ;;
    esac

    inform major "Creating instance disk $instancedisk ($instancedisksize), based on $sourcedisk"

    qemu-img create -f qcow2        \
        -q              \
        -F qcow2        \
        -o backing_file="$sourcedisk" \
        $instancedisk \
        $instancedisksize
}
#}}}

vm_has_dhcp_lease() { #{{{
    # probes the virsh dhcp server to see if the host with mac address $1
    # has acquired a dhcp lease.
    read dhcp_leasedate dhcp_leasetime dhcp_mac dhcp_proto dhcp_ip dhcp_hostname dhcp_duid <<<"$(v net-dhcp-leases --mac "$1" default | grep "$1")"
    [ "$dhcp_ip" ]
} #}}}

wait_for() { #{{{
    saved="`set +o`"
    set +x
    set +e
    spin=0
    maxdelay="$1"
    : ${maxdelay:=30}
    shift
    inform normal "Waiting ($maxdelay seconds) for $*"
    echo -n ":: "
    t0=$(date +%s)
    while wait_for_before_call=$(date +%s) ; ! eval "$*" ; do
        echo -n .
        now="$(date +%s)"
        if [ $now = $wait_for_before_call ] ; then
            sleep 1
        fi
        now="$(date +%s)";
        spin="$((now-t0))"
        if [ "$spin" -ge $maxdelay ] ; then
            echo "timeout waiting for $*" >&2
            v list --all
            du -h --max-depth 2 $TANKER_DATABASE | sort -h
            df -h "$TANKER_DATABASE"
            eval "$saved"
            false
            return
        fi
    done
    echo "ok [waited $spin seconds]"
    eval "$saved"
} #}}}

vm_is_running() {
    [[ "`v domstate $shortid 2>/dev/null`" =~ running ]]
}

vm_is_down() {
    [[ "`v domstate $shortid 2>/dev/null`" =~ shut.off ]]
}

vm_ip_is_here() { #{{{
    ping -q -c 1 -W 1 "${1%/*}" >/dev/null 2>&1
}
#}}}

vm_ssh_options() { #{{{
    echo "-o PreferredAuthentications=publickey -oUserKnownHostsFile=$instance/known_hosts -i $instance/id_$t"
}
#}}}
vm_ssh() { #{{{
    echo ssh `vm_ssh_options` "${ssh_extra[@]}" ${ssh_user:-root}@$instanceip > $instance/vm_ssh.sh
    ssh `vm_ssh_options` "${ssh_extra[@]}" ${ssh_user:-root}@$instanceip "$@"
}
#}}}

vm_ssh_works() { #{{{
    t="${ssh_key_types[0]}"
    [ "$t" ]
    tmp=("${ssh_extra[@]}")
    local ssh_extra
    ssh_extra=("${tmp[@]}" -n)
    vm_ssh : >> $instance/test_vm_ssh_works.log 2>&1
}
#}}}
source_hooks() # {{{ load (but do not call!) machine-specific hooks
{
    declare -n ov="$1"
    try_hooks=()
    for i in "${image_parents[@]}" ; do
        if [[ "$i" =~ ^([^:]*):.*$ ]] ; then
            try_hooks+=("$mydir/specifics/${BASH_REMATCH[1]}.sh")
        fi
        try_hooks+=("$mydir/specifics/$i.sh")
    done
    inform debug "list of hooks to try: ${try_hooks[@]}"
    if [ "${ov["hooks"]}" ] ; then
        if ! [ -r "${ov["hooks"]}" ] ; then
            inform failure "${ov["hooks"]} does not exist"
        fi
        try_hooks+=("${ov["hooks"]}")
    fi
    xml_extra=
    vm_build_xml_description_hooks() { :; }
    vm_build_default_parameter_hooks() { :; }
    vm_build_install_hooks() { :; }
    vm_build_cloud_init_tree_hooks() { :; }
    vm_build_cloud_init_make_xml_bit_hook() { vm_build_cloud_init_make_xml_bit_default "$@" ; }
    vm_build_create_intermediary_image_hook() { :; }

    if ! [ "${ov["no-hooks"]}" ] ; then
        for f in "${try_hooks[@]}" ; do
            if [ -f "$f" ] ; then
                inform normal "Reading specifics file $f"
                source "$f"
            else
                inform debug "No specifics file $f"
            fi
        done
    fi
} # }}}
create_cloud_init_tree() { #{{{
    inform normal "Creating tree for cloud-init"
    ci="$instance/cloud-init"
    mkdir -p "$ci"
    echo "instance-id: $shortid" > "$ci/meta-data"
    keys() {
        for t in "$@" ; do
            f="$instance/id_${t}.pub"
            echo "    - `cat "$f"`"
        done
    }
    # cloudinitcheat is really a miserable hack.
    # It seems that despite what is written here:
    # https://cloudinit.readthedocs.io/en/latest/topics/boot.html#first-boot-determination
    # ... cloud-init makes no effort to determine the instance if its
    # data source is a "no-cloud" data source such as a cd.
    # Our pain goes as follows. 
    # We stick manual_cache_clean: False in the cloud-init user-data, which activates the
    # "check" policy, visible via the message "attempting to read from
    # cache [check]" in the cloud-init logs.
    # But the "current instance id" is fetched from *NO OTHER MEANS* than
    # the /run/cloud-init/.instance-id file. See this excerpt in
    # /usr/local/lib/python3.7/site-packages/cloudinit/stages.py :
    #
    #   def _restore_from_checked_cache(self, existing):
    #      if existing not in ("check", "trust"):
    #          raise ValueError("Unexpected value for existing: %s" % existing)
    # 
    #      ds = self._restore_from_cache()
    #      if not ds:
    #          return (None, "no cache found")
    # 
    #      run_iid_fn = self.paths.get_runpath('instance_id')
    #      if os.path.exists(run_iid_fn):
    #          run_iid = util.load_file(run_iid_fn).strip()
    #      else:
    #          run_iid = None
    # 
    #      if run_iid == ds.get_instance_id():
    #          return (ds, "restored from cache with run check: %s" % ds)
    #      elif existing == "trust":
    #          return (ds, "restored from cache: %s" % ds)
    #      else:
    #          if (hasattr(ds, 'check_instance_id') and
    #                  ds.check_instance_id(self.cfg)):
    #              return (ds, "restored from checked cache: %s" % ds)
    #          else:
    #              return (None, "cache invalid in datasource: %s" % ds)
    #
    #
    ##
    ## I'm not claiming that it's awfully buggy in all cases, but at least
    ## in my case of a local data source on freebsd, it just doesn't work.
    ## Logs:
    # 2021-02-11 17:48:13,171 - handlers.py[DEBUG]: start: init-network/check-cache: attempting to read from cache [trust]
    # 2021-02-11 17:48:13,172 - util.py[DEBUG]: Reading from /var/lib/cloud/instance/obj.pkl (quiet=False)
    # 2021-02-11 17:48:13,172 - util.py[DEBUG]: Read 20457 bytes from /var/lib/cloud/instance/obj.pkl
    # 2021-02-11 17:48:13,175 - util.py[DEBUG]: Reading from /run/cloud-init/.instance-id (quiet=False)
    # 2021-02-11 17:48:13,189 - util.py[DEBUG]: Read 16 bytes from /run/cloud-init/.instance-id
    # 2021-02-11 17:48:13,190 - stages.py[DEBUG]: restored from cache with run check: DataSourceNoCloud [seed=/dev/iso9660/cidata][dsmode=net]
    #
    # A workaround is to create a service that sneaks in before
    # cloud-init starts, and surreptitiously *removes* the file
    # /run/cloud-init/.instance-id. This forces cloud-init to find out
    # the instance id in another way (by mounting the CD, probably),
    # which will do what we want, eventually.
    #
    # Our workaround in action:
    # cloud-init cheater in action
    # /usr/local/bin/cloud-init startingWARN: no logging configured! (tried 0 configs)
    # Setting up basic logging...
    # Cloud-init v. 20.4.1 running 'init-local' at Thu, 11 Feb 2021 18:40:51 +0000. Up 2.26381516456604 seconds.
    # 2021-02-11 18:40:51,754 - util.py[DEBUG]: Cloud-init v. 20.4.1 running 'init-local' at Thu, 11 Feb 2021 18:40:51 +0000. Up 2.26381516456604 seconds.
    # 2021-02-11 18:40:51,755 - main.py[DEBUG]: No kernel command line url found.
    # 2021-02-11 18:40:51,756 - main.py[DEBUG]: Closing stdin.
    # 2021-02-11 18:40:51,760 - util.py[DEBUG]: Writing to /var/log/cloud-init.log - ab: [644] 0 bytes
    # 2021-02-11 18:40:51,763 - util.py[DEBUG]: Changing the ownership of /var/log/cloud-init.log to 0:0
    # 2021-02-11 18:40:51,764 - util.py[DEBUG]: Attempting to remove /var/lib/cloud/instance/boot-finished
    # 2021-02-11 18:40:51,771 - util.py[DEBUG]: Attempting to remove /var/lib/cloud/data/no-net
    # 2021-02-11 18:40:51,772 - handlers.py[DEBUG]: start: init-local/check-cache: attempting to read from cache [check]
    # 2021-02-11 18:40:51,773 - util.py[DEBUG]: Reading from /var/lib/cloud/instance/obj.pkl (quiet=False)
    # 2021-02-11 18:40:51,775 - util.py[DEBUG]: Read 20378 bytes from /var/lib/cloud/instance/obj.pkl
    # 2021-02-11 18:40:51,782 - util.py[DEBUG]: Reading from /var/lib/cloud/seed/nocloud/meta-data (quiet=False)
    # 2021-02-11 18:40:51,783 - util.py[DEBUG]: Reading from /var/lib/cloud/seed/nocloud-net/meta-data (quiet=False)
    # 2021-02-11 18:40:51,784 - stages.py[DEBUG]: cache invalid in datasource: DataSourceNoCloud [seed=/dev/iso9660/cidata][dsmode=net]
    # 
    #
    # A better workaround could perhaps be to modify the
    # DataSourceNoCloud, but it isn't clear how (apparently
    # check_instance_id is checked too late in the process)

    cat > "$ci/user-data" <<EOF
#cloud-config
merge_how:
 - name: list
   settings: [append]
 - name: dict
   settings: [no_replace, recurse_list]

manual_cache_clean: False

preserve_hostname: false
# fqdn: <fqdn>
hostname: $shortid
manage_etc_hosts: localhost

write_files:
  - encoding: gzip
    content: !!binary |
        H4sIAJ95JWAAA4VQPU/DMBCde7/icDvAkFqskTqACFAhNSUqMEbOxSWWHBs5Dgvlv+
        PEVVNYWKz35ae7m1/wShneNQBz3Bb56/ouS5G07WtllKdGCh+cInt+WRfB0TVZs1fv
        2NreeHLKa0tCY8jcZvf5EMFNtnvLi6f15mEqiqkTjehYNeK9MkIDLJFLT9zRsusrB7
        C92T2uGO87x8cK3oVx0zN+opMxgkjDwwCMaOWK/V6KQeeF8yW19dEa5XJUB9N+RC9l
        4OhTuLOCUhpRaflPBfyVLq/gC2aSGosxnsRTDAnpUBkU5JUNE89ci8keuesNn5J8qU
        woMiQTVePhgCl8A2gr6tJReTzmYlgWIPyMYtsKUyNbXDP4AZ2bPVXsAQAA
    path: /usr/local/etc/rc.d/cloudinitcheat
    permissions: '0755'
    owner: root:wheel

EOF
    cat > "$ci/users.yaml" <<EOF
#cloud-config
disable_root: 0
ssh_authorized_keys:
$(keys "${ssh_key_types[@]}")
users:
 - default
 - name: root
   ssh_redirect_user: false
   #    lock-passwd: false
   #    # \`openssl passwd -6 password\`
   #    passwd: \$6\$zjE9rXZU6vlYnluO\$Xka2NXmUhq7bADOMz0goyn0PbHdGSFPdjSj.WTGvIU1.ok2xqpmNtXDpYDKR6Jv9XF/QLJ19cm8Nc6Vg3b49d/
   ssh_redirect_user: false
   ssh_authorized_keys:
$(keys "${ssh_key_types[@]}")
 - name: user
   ssh_authorized_keys:
$(keys "${ssh_key_types[@]}")
EOF
    cat > "$ci/finish.yaml" <<EOF
#cloud-config

# write_files:
#   - path: /etc/cloud/cloud-init.disabled
#     owner: root:root
#     permissions: '0644'

# this powers off the machine on "vm run", which doesn't fit well with
# what we want to do now.

### doesn't work with centos ?? (runcmd /sbin/poweroff does work)
## and anyway, what if we change our mind ?
#power_state:
#  mode: poweroff
#  message: Bye
#  condition: True
EOF
    hkeys() {
        for t in "$@" ; do
            priv="$instance/host_id_${t}"
            pub="$priv.pub"
            cat <<EOF
    ${t}_private: |
$(sed -e "s,^,        ,g" "$priv")
    ${t}_public: $(cat $pub)
EOF
        done
    }
    cat > "$ci/sshd.yaml" <<EOF
#cloud-config
ssh_deletekeys: true
ssh_genkeytypes: "${ssh_key_types[@]}"
allow_public_ssh_keys: true
ssh_publish_hostkeys:
    enabled: true
    blacklist: [dsa, ecdsa]
ssh_keys:
$(hkeys "${ssh_key_types[@]}")
EOF
    vm_build_cloud_init_tree_hooks

    cat "$ci"/*.yaml >> "$ci/user-data"
} #}}}
vm_build_cloud_init_make_xml_bit_default() { #{{{
    cat <<EOF
    <disk type='file' device='disk'>
            <driver name='qemu' type='raw'/>
            <source file='$1'/>
            <target dev='vdb' bus='virtio'/>
            <readonly/>
        </disk>
EOF
}
#}}}
create_cloud_init_iso_from_tree() { #{{{
    inform normal "Creating iso for cloud-init"
    # is "cidata" specific for cloud_init data, or just useless ? IDK
    (cd "$ci"; genisoimage -o "$ci.iso" -V cidata -J user-data meta-data > /dev/null 2>&1)
    xml_devices+=(["cloud_init_iso"]="$(vm_build_cloud_init_make_xml_bit_hook "$ci.iso")")
} #}}}

random_mac() #{{{
{
    m=()
    m+=($((RANDOM % 256)))
    m+=($((RANDOM % 256)))
    m+=($((RANDOM % 256)))
    m+=($((RANDOM % 256)))
    printf "52:54:%02x:%02x:%02x:%02x" "${m[@]}"
}
#}}}

# 

up_machine()
{
    shortid="$(sanitize_id "$1")"
    instance="$TANKER_DATABASE/images/$shortid"
    read instancemac < "$instance/macaddr"
    : ${boot_delay=30}
    : ${ping_delay=30}
    : ${sshd_delay=30}
    # v start --console $shortid
    if ! [[ "$(v domstate $shortid)" =~ running ]] ; then
        v start $shortid
    fi
    wait_for "${boot_delay}" vm_has_dhcp_lease "$instancemac"
    instanceip="${dhcp_ip%/*}"
    inform major "Host $shortid has acquired IP $dhcp_ip (dhcp hostname: $dhcp_hostname)"
    if [ "${#ssh_key_types[@]}" = 0 ] ; then
        ssh_key_types=()
        for f in $instance/host_id_*.pub ; do
            t=$(basename "$f" .pub)
            t="${t#host_id_}"
            ssh_key_types+=("$t")
        done
    fi
    for t in "${ssh_key_types[@]}" ; do
        echo "$shortid,$instanceip $(cat $instance/host_id_$t.pub)"
    done > "$instance/known_hosts"
    wait_for "${ping_delay}" vm_ip_is_here "$instanceip"
    wait_for "${sshd_delay}" vm_ssh_works
}

# remove_cd() { #{{{
#     inform normal "removing CD"
#     set +e
#     i=0
#     while !  v detach-disk --domain $shortid $ci.iso --persistent --config ; do
#         let i+=1
#         if [ $i -gt 5 ] ; then
#             inform error "giving up on removing CD"
#             break
#         fi
#         sleep 1
#         inform normal "removing CD (still trying)"
#     done
#     set -e
# }
# #}}}
# 

vm_build_backend_preconditions() {
    if ! [ "$pulled_image_checksum" ] ; then return ; fi
    echo "$pulled_image_checksum"
    echo "$instancedisksize"
    echo "$IMAGENAME_AND_TAG"
}

# The build backend sets several variables relative to the instance.
vm_build_backend() {
    vm_build_backend_start=$(date +%s)

    # This is the origin distro.  We set IMAGENAME and TAG (or TAG
    # empty).  We need this early in order to know which of the specifics
    # hooks file to load
    IMAGENAME_AND_TAG="$1"
    imagename_extract_tag "$IMAGENAME_AND_TAG"
    shift

    # Target vm name. This *may* be set as the empty string, in which
    # case a randomly set name is chosen.
    TARGET_VM_NAME="$1"
    shift

    # read all bits from the command line, before we use the hooks to set
    # any family-dependent defaults.
    ncpus="${vm_build_backend_options_values["cpus"]}"
    instanceram="${vm_build_backend_options_values["memory"]}"
    chipset="${vm_build_backend_options_values["chipset"]}"
    instancedisksize="${vm_build_backend_options_values["disksize"]}"

    ### IMAGENAME, TAG, url, shortid, mac, instancedisk {{{
    # sets IMAGENAME and TAG ; also set url
    #### random mac address, and machine ID
    instancemac=$(random_mac)
    tmp=${instancemac#52:54:}
    tmp=${tmp//:/}
    shortid="${TARGET_VM_NAME}"
    if ! [ "$shortid" ] ; then
        shortid="${IMAGENAME_AND_TAG}-$tmp"
    fi
    shortid=$(sanitize_id $shortid)

    instance="$TANKER_DATABASE/images/$shortid"
    if [ -d "$instance" ] || v domstate $shortid > /dev/null 2>&1 ; then
        if [ "${vm_build_backend_options_values["replace"]}" ] ; then
            inform major "Removing previous vm named $shortid"
            v destroy $shortid || :
            v undefine $shortid || :
            rm -rf "$instance"
        else
            inform failure "A vm with name $shortid already exists"
        fi
    fi
    mkdir -p "$instance"
    echo "$instancemac" > "$instance/macaddr"
    instancedisk="$instance/disk.qcow2"
    #}}}


    pull_command "${IMAGENAME_AND_TAG}"

    inform major "Pulled image is $pulled_image"
    f="${pulled_image%.qcow2}.checksum"
    if [ -f "$f" ] ; then
        read pulled_image_checksum < "$f"
        inform major "Pulled image checksum is $pulled_image_checksum"
    else
        unset pulled_image_checksum
    fi

    source_hooks vm_build_backend_options_values

    vm_build_default_parameter_hooks

    # {{{ prepare all xml bits. This is quick.

    ### xml bit for number of cpus.{{{
    xml_vcpus="<vcpu placement='static'>$ncpus</vcpu>"
    #}}}

    ### xml bit for cpu passthrough {{{
    xml_cpu="<cpu mode=\"host-passthrough\" check=\"none\" migratable=\"on\"/>"
    #}}}

    ### xml bit for memory{{{
    read ramnumber ramunit < <(echo $instanceram | perl -ne '/(\d+)(\D+)$/ && print "$1 $2";')
    if [ "$ramunit" ] ; then
        xml_memory="<memory unit='$ramunit'>$ramnumber</memory>"
    else
        xml_memory="<memory>$instanceram</memory>"
    fi
    xml_memory="$xml_memory${xml_memory//memory/currentMemory}"
    #}}}

    ### xml bit for chipset {{{
    if [ "$chipset" = default ] ; then
        machine=
    else
        machine=" machine='q35'"
    fi
    xml_chipset="<os><type arch= \"x86_64\"$machine>hvm</type></os>"
    #}}}

    ### xml bits for clock and power {{{
    # xml_clock="<clock offset=\"localtime\"/>"
    xml_clock="<clock offset=\"timezone\" timezone=\"$(date +%Z)\"/>"
    xml_power=
    xml_power+="<on_poweroff>destroy</on_poweroff>"
    xml_power+="<on_reboot>restart</on_reboot>"
    xml_power+="<on_crash>destroy</on_crash>"
    #}}}

    ### xml bits for all devices {{{
    declare -A xml_devices
    xml_devices+=(["emulator"]="<emulator>/usr/bin/kvm</emulator>")
    xml_devices+=(["instancedisk"]="<disk type='file' device='disk'>
            <driver name='qemu' type='qcow2'/>
            <source file='$instancedisk'/>
            <target dev='vda' bus='virtio'/>
        </disk>
    ")
    if [[ $LIBVIRT_NETWORK_TYPE =~ network:(.*) ]] ; then
        ln_type=network
        ln_source="<source network='default'/>"
    else
        ln_type="$LIBVIRT_NETWORK_TYPE"
    fi
    xml_devices+=([network]="   <interface type='$ln_type'>
            $ln_source
            <mac address='$instancemac'/>
            <target dev='vnet0'/>
            <model type='e1000'/>
            <alias name='net0'/>
        </interface>
    ")
#    <interface type="network">
#  <mac address="52:54:00:06:f3:6c"/>
#  <source network="default" portid="ce0dc2b2-5d7b-4f14-a80e-192435837a49" bridge="virbr0"/>
#  <target dev="vnet54"/>
#  <model type="e1000"/>
#  <alias name="net0"/>
#  <address type="pci" domain="0x0000" bus="0x00" slot="0x02" function="0x0"/>
#</interface>
    xml_devices+=([console]="   <serial type='pty'>
            <target type='isa-serial' port='0'>
            <model name='isa-serial'/>
            </target>
        </serial>
        <console type='pty'>
            <target type='serial' port='0'/>
        </console>
    ")
    # dunno about the virtio-serial thing maybe it's something good to have -->
    xml_devices+=(["virtio_input_channel_serial"]="   <channel type='unix'>
          <source mode='bind' />
          <target type='virtio' name='org.qemu.guest_agent.0' state='disconnected'/>
          <alias name='channel0'/>
          <address type='virtio-serial' />
        </channel>
    ")
    xml_devices+=(["virtio_input_channel_spice"]="   <channel type='spicevmc'>
          <target type='virtio' name='com.redhat.spice.0' state='disconnected'/>
          <alias name='channel1'/>
        </channel>
        <graphics type='spice' autoport='yes' listen='127.0.0.1'>
          <listen type='address' address='127.0.0.1'/>
          <image compression='off'/>
        </graphics>
    ")

    xml_devices+=(["tablet"]="   <input type='tablet' bus='usb'>
          <alias name='input0'/>
          <address type='usb' bus='0' port='1'/>
        </input>
    ")
    xml_devices+=(["mouse"]="   <input type='mouse' bus='ps2'>
          <alias name='input1'/>
        </input>
    ") 
    xml_devices+=(["keyboard"]="   <input type='keyboard' bus='ps2'>
          <alias name='input2'/>
        </input>
    ") 
    xml_devices+=(["sound"]="   <sound model='ich9'>
          <alias name='sound0'/>
          <address type='pci'/>
        </sound>
    ") 
    xml_devices+=(["video"]="   <video>
          <model type='qxl' ram='65536' vram='65536' vgamem='16384' heads='1' primary='yes'/>
          <alias name='video0'/>
          <address type='pci' />
        </video>
    ") 
    # we want a kernel with rng support ! NOT the debian cloud kernel !
    xml_devices+=(["rng"]="   <rng model=\"virtio\">
          <backend model=\"random\">/dev/urandom</backend>
        </rng>
    ") 
    #}}}

    vm_build_xml_description_hooks

    #}}}

    inform major "Preparing install of $shortid with ram=${ramnumber}${ramunit}, ncpus=$ncpus chipset=$chipset"

    create_instance_disk        "$url" "$instancedisk"

    inform major "Preparing provisioning information (cloud-init)" # {{{
    ssh_key_types=(rsa ed25519)
    # prepare ssh keys (host)
    ssh-keygen -C "" -N "" -t rsa -b 2048 -q -f $instance/host_id_rsa
    ssh-keygen -C "" -N "" -t ed25519 -q -f $instance/host_id_ed25519
    # prepare ssh keys (users)
    ssh-keygen -C "" -N "" -t rsa -b 2048 -q -f $instance/id_rsa
    ssh-keygen -C "" -N "" -t ed25519 -q -f $instance/id_ed25519

    create_cloud_init_tree
    create_cloud_init_iso_from_tree
    # }}}

    install_delay=30
    vm_build_install_hooks      # can be used to tweak delays

    inform major "Defining and booting vm"
    # {{{ defining
    inform debug "VM devices: ${!xml_devices[@]}"

    read -d'' -r instancexml <<EOF
<domain type='kvm'>
 <name>$shortid</name>
 $xml_memory
 $xml_vcpus
 $xml_cpu
 $xml_chipset
 $xml_clock
 $xml_power
 $xml_extra
 <devices>
   ${xml_devices[@]}
 </devices>
</domain>

EOF

    instancexmlfile="$instance/desc.xml"

    echo "$instancexml" > "$instancexmlfile"
    
    # how="persistent: virsh define+start"
    # how="transient: virsh create"

    # TODO: register for an ip !
    # TODO: persistent / non-persistent
    # virsh create is for transient config
    v define $instancexmlfile
    # }}}

    boot_delay="$install_delay" up_machine "$shortid" "$instancemac"

    # if we remove the CD, then we must make sure to disable cloud-init
    # on the next reboot, because the machine will take ages to boot
    # without a cd.
    #
    # on the other hand, if we disable cloud-init, it's harder to derive
    # a new virtual machine from this one.
    #
    # Bottom line: keep the cd. The downside is that the cd contains
    # cleartext key material, but that is accessible to root only anyway.
    #
    # remove_cd

    vm_build_time=$(($(date +%s)-vm_build_backend_start))
    inform major "Build complete (preparation time: $vm_build_time seconds)"
}

# The wrap_vm_ssh function ssh's a command onto the guest.
#
# Each command is a sequence of tokens. The first few tokens in the
# sequence determine how the command is executed. Tokens are understood
# in that order:
#  - optional host/guest token
#  - optional username token
#  - optional context token
#  - script and arguments.
#
# The host/guest tokens are "@host" and "@guest". When the "@host" token
# is present, material will be copied from the host to the guest, and
# executed there.
#
# The username token is any token that matches ^([^@]+)@$ (anything that
# ends with an @). It determines the user under which the command will be
# run.
#
# The context token makes sense only when the "@host" token is present.
# It determines what material should be copied on the guest.
#
#  - If the context token is a path to a file ending in .sh, this file is
#    (copied to and) executed on the guest with the provided arguments.
#    Note that the name of the script itself is changed to a
#    randomly-generated name. In this case, the context is really the
#    script itself, and the rest of the command line only gives its
#    arguments.
#
#  - If the context token is a path to a file ending in .tar.gz, this
#    archive is decompressed on the guest in a temporary directory. The
#    script whose name is provided after the tar file (defaulting to
#    ./script.sh if there is none) is executed from that directory, with
#    the rest of the arguments.
# 
#  - If the context token is a path to a directory, the directory
#    contents are copied to a randomly-generated directory under /tmp on
#    the guest. The script whose name is provided after the directory
#    name (defaulting to script.sh if there is none) is executed, with
#    the rest of the arguments.
#
#
# When there is no host/guest token at the beginning, the following rule
# is used:
#  - An absolute path means that @guest is implicit
#  - A relative path means that @host is implicit

# Note that for guest commands, the shell environment is **NOT** propagated
# to the client. You must arrange so that the material you have in your
# environment is passed in another way. For example, this sequence may be
# used to propagate all shell settings that are present in the bash array
# called "exports":
#        @guest user@ archive.tar.gz env "${exports[@]}" make

wrap_vm_ssh()
{
    # The exit code of this command **should** be the exit code of the
    # remote command. I have doubts that this actually works as intended,
    # though.
    ssh_user=root
    if [ "$1" = "@guest" ] ; then
        shift
        if [[ $1 =~ ^([^@]+)@$ ]] ; then ssh_user="${BASH_REMATCH[1]}" ; shift ; fi
        vm_ssh "${ssh_extra[@]}" "$@"
        return
    fi
    if [[ $1 =~ ^([^@]+)@$ ]] ; then ssh_user="${BASH_REMATCH[1]}" ; shift ; fi
    if [[ "$1" =~ ^(/.*)$ ]] ; then
        vm_ssh "${ssh_extra[@]}" "$@"
        return
    fi
    if [ "$1" = "@host" ] ; then shift ; fi
    if [[ $1 =~ ^([^@]+)@$ ]] ; then ssh_user="${BASH_REMATCH[1]}" ; shift ; fi
    random=$(uuidgen  | sha256sum | cut -c1-10)
    if [[ "$1" =~ \.sh$ ]] ; then
        scp `vm_ssh_options` -p "$1" "${ssh_user}@$instanceip":"/tmp/$random.sh"
        shift
        vm_ssh "/tmp/$random.sh" "$@"
    elif [[ "$1" =~ \.tar.gz$ ]] ; then
        scp `vm_ssh_options` -p "$1" "${ssh_user}@$instanceip":"/tmp/$random.tar.gz"
        vm_ssh mkdir "/tmp/$random"
        vm_ssh "cd /tmp/$random ; tar xzf -" < "$1"
        shift
        script="$1"
        : ${script:=./script.sh}
        shift || :
        vm_ssh "cd /tmp/$random ; $script" "$@"
    elif [ -d "$1" ] ; then
        vm_ssh mkdir "/tmp/$random"
        # scp is incapable of copying "." for instance, and traverses
        # symlinks. we'd like to use rsync, but that would mean always
        # pull an extra package. I think tar will do.
        (cd "$1" ; tar cf - .) | vm_ssh "cd /tmp/$random ; tar xf -"
        # scp `vm_ssh_options` -pr "$1" "${ssh_user}@$instanceip":"/tmp/$random/"
        shift
        script="$1"
        : ${script:=./script.sh}
        shift || :
        vm_ssh "cd /tmp/$random ; $script" "$@"
    else
        inform error "Unexpected host command format: $1"
    fi
}

wrap_several_vm_sshs()
{
    x=()
    success=1
    if [ $# = 0 ] ; then return; fi
    while true ; do 
        if [ "$1" = -- ] || [ $# = 0 ] ; then
            inform major "Running command: ${x[@]}"
            if ! wrap_vm_ssh "${x[@]}" ; then
                unset success
            fi
            x=()
            if [ $# = 0 ] ; then break; fi
        else
            x+=("$1")
        fi
        shift
    done
    [ "$success" ]
}

vm_commands+=([run]="run a command in a new vm")
declare -A vm_run_options
vm_run_options+=(["rm"]="Automatically remove the vm when it exits")
vm_run_options+=(["tty,t"]="Allocate a tty")
# note that this does not work
# vm_build_options+=("${vm_build_or_run_options[@]}")
#
# simple reproducer:
# Z=(A 1 B 2); declare -A X; X+=("${Z[@]}"); echo "${!X[@]}"
expand_associative_array vm_run_options vm_build_or_run_options
vm_run_description_arguments="IMAGENAME[:TAG] [COMMAND [ARGUMENTS...]]"
vm_run_command() {

    declare -n vm_build_backend_options_values=vm_run_options_values
    vm_build_backend "$1" ""
    shift

    if [ $# = 0 ] ; then set -- "/bin/sh" ; fi

    persistent=
    if ! [ "${vm_run_options_values["rm"]}" ] ; then persistent=1 ; fi

    ssh_extra=()
    if [ "${vm_run_options_values["tty"]}" ] ; then ssh_extra+=("-t") ; fi

    args=("$@")

    # schedule a poweroff
    if [ ${#args[@]} -gt 0 ] ; then args+=(--) ; fi
    args+=("/sbin/poweroff")

    local rc

    wrap_several_vm_sshs "${args[@]}"

    rc=$?

    if ! [ "$rc" = 0 ] ; then
        inform error "Our main command line returned an error code $rc.  We'll first (try to) clean up after ourselves"
    fi

    if ! wait_for 30 vm_is_down ; then
        inform major "Forcibly shutting down vm as a last resort"
        v destroy $shortid || :
    fi

    # inform major "Shutting down vm"
    # v destroy $shortid || :
    if ! [ "$persistent" ] ; then
        inform major "Deleting vm"
        v undefine $shortid
        rm -rf $instance
    else
        inform major "Persistent vm, kept installed as $shortid (disk: $instancedisk)"
    fi

    if ! [ "$rc" = 0 ] ; then
        inform error "Exiting with error code $rc that came from our main command line"
    fi
    [ "$rc" = 0 ]
}


vm_commands+=([build]="build a new vm")
declare -A vm_build_options
vm_build_options+=(["tag,t:string"]="name (and tag) of vm")
read -d'' -r vm_build_description <<'EOF'
This command builds a VM and keeps it for further use. Several scripts
can be run, separated by -- on the command line.

EOF
vm_build_description_arguments="SOURCEIMAGENAME[:TAG] [COMMAND [ARGUMENTS...]]"
expand_associative_array vm_build_options vm_build_or_run_options
vm_build_command()
{
    declare -n vm_build_backend_options_values=vm_build_options_values
    vm_build_backend "$1" "${vm_build_options_values["tag"]}"
    shift

    ssh_extra=()

    args=("$@")

    # we want to mark our machines as re-provisionable. However, there's
    # a catch, because cloud-init is somewhat buggy in this regard. See
    # the hack in create_cloud_init_tree, which does most of the work.
    # There's not much we can do here without significant help from
    # there.
    # if [ ${#args[@]} -gt 0 ] ; then args+=(--) ; fi
    # args+=("@guest" rm -rf /var/lib/cloud)
    touch "$instance/cloudinit_ready"

    # # print the contents of rc.conf for debugging
    # if [ ${#args[@]} -gt 0 ] ; then args+=(--) ; fi
    # args+=("@guest" "cat" "/etc/rc.conf")

    # schedule a poweroff
    if [ ${#args[@]} -gt 0 ] ; then args+=(--) ; fi
    args+=("@guest" "root@" "/sbin/poweroff")

    wrap_several_vm_sshs "${args[@]}"

    if ! wait_for 30 vm_is_down ; then
        inform major "Forcibly shutting down vm as a last resort"
        v destroy $shortid || :
    fi
}

vm_commands+=(["ssh"]="ssh into an existing machine")
vm_ssh_description_arguments="VMNAME"
declare -A vm_ssh_options
vm_ssh_options+=(["user,u:username"]="(root) Connect as this user")
vm_ssh_options+=(["shutdown"]="Power off the vm after running the command")
vm_ssh_options+=(["sequence,S"]="Interpret argument as a sequence of commands as in vm build / vm run")
read -d'' -r vm_build_description <<'EOF'
This command runs an arbitrary ssh command on the vm, powering it up if
need be. Note that the default behaviour is to keep the machine up
afterwards, unless --shutdown is passed.

By default, the command is run simply on the host, with no bells and
whistles. If you wish to wield the power of the @host markers as in vm
build / vm run, use the --sequence flag.

EOF
vm_ssh_command()
{
    VMNAME="${1}"
    ssh_user="${vm_ssh_options_values["user"]}"
    shift
    : ${VMNAME?missing}
    if ! [ -d "$TANKER_DATABASE/images/$(sanitize_id "$VMNAME")" ] ; then
        inform failure "No vm by name $VMNAME"
    fi
    up_machine "$VMNAME"
    if [ "${vm_ssh_options_values["sequence"]}" ] ; then
        if [ $# = 0 ] ; then set -- "/bin/sh" ; fi
        wrap_several_vm_sshs "$@"
    else
        inform normal "ssh command line is: ssh `vm_ssh_options` ${ssh_user:-root}@$instanceip"
        vm_ssh "$@"
    fi
    if [ "${vm_ssh_options_values["shutdown"]}" ] ; then
        # we have a problem here. Even destroy --graceful is apparently
        # pulling the cord. This means that that no "sync" is called, and
        # some files may be corrupted, especially among the last few that
        # got written to. Among these, sshd_config pops up as being
        # problematic (because failure means that it gets truncated). It
        # seems much more robust to ssh a poweroff command instead.
        wrap_vm_ssh "@guest" "root@" /sbin/poweroff
        # v destroy --graceful "$VMNAME"
        if ! wait_for 30 vm_is_down ; then
            inform major "Forcibly shutting down vm as a last resort"
            v destroy $shortid || :
        fi
    fi
}

vm_commands+=(["list"]="list vms currently defined")
vm_list_command()
{
    database_mkdirs
    (cd "$TANKER_DATABASE/images" ; find . -mindepth 1 -maxdepth 1 -a -type d | xargs -r du -sh)
}

vm_commands+=(["kill"]="kill an existing vm") # {{{
vm_kill_description_arguments="VMNAME"
vm_kill_command()
{
    VM_NAME="$1"
    : ${VM_NAME:?missing}
    if v list --name | grep -q "^$VM_NAME\$" ; then
        if v destroy "$VM_NAME" ; then
            inform normal "Destroyed vm $VM_NAME"
        else
            inform error "vm destroy $VM_NAME failed"
        fi
    else
        inform normal "No running vm by name $VM_NAME"
    fi
    if v list --name --all | grep -q "^$VM_NAME\$" ; then
        if v undefine "$VM_NAME" ; then
            inform normal "Undefined vm $VM_NAME"
        else
            inform error "vm undefine $VM_NAME failed"
        fi
    else
        inform normal "No defined vm by name $VM_NAME"
    fi
    saneVM_NAME=$(sanitize_id "$VM_NAME")
    if [ -d "$TANKER_DATABASE/images/$saneVM_NAME" ] ; then
        rm -rf "$TANKER_DATABASE/images/$saneVM_NAME"
        inform normal "Wiped out: $TANKER_DATABASE/images/$saneVM_NAME"
    else
        inform normal "No vm data by name $VM_NAME"
    fi
} # }}}

# #{{{ dead excerpt using virt-install
#         echo ":: installing vm (virt-install --import)"
# 
#         case $model in
#             fedora31)   osvariant=fedora30;;
#             *) osvariant=$model;;
#         esac
# 
#         if [ "$ramunit" ] ; then
#             case "$ramunit" in
#                 G|GiB) instanceram=$((ramnumber*1024));;
#                 M|MiB) instanceram=$ramnumber;;
#                 *) echo "weird ram unit $ramunit" >&2 ; exit 1;;
#             esac
#         fi
#         install_args=( \
#             --connect qemu:///system
#             --os-variant $osvariant
#             -n $shortid
#             -r $instanceram
#             --vcpus $instancecpus
#             --network network=default,mac=$instancemac
#             --import
#             --disk $instancedisk
#             --disk $overlayiso,device=cdrom
#         )
#         set -- --nographics
#         time virt-install "${install_args[@]}" "$@"
# } #}}}


# maybe someday.

# commands+=("context"    "Manage contexts")
# commands+=("manifest"   "Manage Tanker image manifests and manifest lists")
# commands+=("system"     "Manage Tanker")
# commands+=("volume"     "Manage volumes")
# commands+=("attach"     "Attach local standard input, output, and error streams to a running vm")
# commands+=("build"      "Build an image from a Dockerfile")
# commands+=("commit"     "Create a new image from a vm's changes")
# commands+=("cp"         "Copy files/folders between a vm and the local filesystem")
# commands+=("create"     "Create a new vm")
# commands+=("diff"       "Inspect changes to files or directories on a vm's filesystem")
# commands+=("events"     "Get real time events from the server")
# commands+=("exec"       "Run a command in a running vm")
# commands+=("export"     "Export a vm's filesystem as a tar archive")
# commands+=("history"    "Show the history of an image")
# 
# commands+=("import"     "Import the contents from a tarball to create a filesystem image")
# commands+=("info"       "Display system-wide information")
# commands+=("inspect"    "Return low-level information on Docker objects")
# commands+=("kill"       "Kill one or more running vms")
# commands+=("logs"       "Fetch the logs of a vm")
# commands+=("port"       "List port mappings or a specific mapping for the vm")
# commands+=("ps"         "List vms")
# commands+=("rename"     "Rename a vm")
# commands+=("restart"    "Restart one or more vms")
# commands+=("rm"         "Remove one or more vms")
# commands+=("rmi"        "Remove one or more images")
# commands+=("run"        "Run a command in a new vm")
# commands+=("start"      "Start one or more stopped vms")
# commands+=("stats"      "Display a live stream of vm(s) resource usage statistics")
# commands+=("stop"       "Stop one or more running vms")
# commands+=("top"        "Display the running processes of a vm")
# commands+=("update"     "Update configuration of one or more vms")
# commands+=("version"    "Show the Tanker version information")
# commands+=("wait"       "Block until one or more vms stop, then print their exit codes")








process_command_line "$@"

# : ${model=$shortid}
# 
# instancecpus=4
# instanceram=4G
# disksize=8G
# instancedisk=$instances_dir/$shortid.qcow2
# instancekeys=$instances_dir/$shortid.hostkeys
# instancexml=$instances_dir/$shortid.xml
# # instancemac and instanceip are obtained from the config files
# 
# 
# tmp=`mktemp -d /tmp/vmXXXXXXXXXXXX`
# if [ "$debug" ] ; then
#     trap "echo 'temp data kept in $tmp'" EXIT
#     set -x
# else
#     trap "rm -rf $tmp" EXIT
# fi
# 
# if [ ${#all_auth_keys[@]} = 0 ] ; then
#     echo ":: No authentication key defined. Creating a temp key"
#     ssh-keygen -t ed25519 -N "" -f $tmp/auth
#     all_auth_keys+=("$tmp/auth")
# fi
# 
# # Use the first provided -k as auth material. Other keys will still be
# # accepted nevertheless
# auth_key="${all_auth_keys[0]}"
# 
# # maybe add more keys to "${all_auth_keys[@]}" ?
# # It's probably easier to just feed several -k args.
# 
# 
# 
# maybe_reenable_debug() { #{{{
#     if [ "$debug" ] ; then
#         set -x
#     fi
# }
# #}}}
# config_check() { #{{{
#     echo ":: retrieving implicit variables"
#     image_url=`python3 $scriptdir/list_os_cloud_images.py $model x86_64 qcow2`
#     case "$model" in
#         freebsd130)
#             read major minor < <(echo "$model" | perl -ne '/freebsd(\d\d)(\d)$/ && print "$1 $2\n";')
#             image_url=http://ftp.freebsd.org/pub/FreeBSD/snapshots/VM-IMAGES/$major.$minor-CURRENT/amd64/Latest/FreeBSD-$major.$minor-CURRENT-amd64.qcow2.xz
#             ;;
#         freebsd1[0-9][0-9])
#             # https://bugs.freebsd.org/bugzilla/show_bug.cgi?id=236922
#             read major minor < <(echo "$model" | perl -ne '/freebsd(\d\d)(\d)$/ && print "$1 $2\n";')
#             image_url=https://download.freebsd.org/ftp/releases/VM-IMAGES/$major.$minor-RELEASE/amd64/Latest/FreeBSD-$major.$minor-RELEASE-amd64.qcow2.xz
#             ;;
#     esac
# 
#     # extra images
#     #
#     # freebsd 11.2 11.3 12.0 12.1
#     # https://download.freebsd.org/ftp/releases/VM-IMAGES/12.1-RELEASE/amd64/Latest/FreeBSD-12.1-RELEASE-amd64.qcow2.xz
#     #
#     # https://wiki.qemu.org/Hosts/BSD seems to be a no-go for openbsd and
#     # netbsd
# 
#     read _ instancemac instanceip < <(grep "^$shortid\\b" $scriptdir/machines.list)
# 
#     for a in image_url instancemac instanceip ; do
#         if ! [ "${!a}" ] ; then echo "no $a" >&2 ; exit 1 ; fi
#         echo ":: $a ${!a}"
#     done
# } #}}}
# dhcp_check() { #{{{
#     echo  ":: making sure libvirt knows our mac in dhcp"
#     $scriptdir/fill-dhcp.sh < $scriptdir/machines.list >/dev/null
# }
# #}}}
# 
# 
# 
# vm_state() {
#     s="`v domstate $shortid 2>/dev/null`"
#     echo "$s"
# }
# 
# boot_vm() { #{{{
#     echo ":: booting VM"
#     case "$(vm_state)" in
#         running) echo ":: (already running)";;
#         shut*off)
#             v start $shortid
#             ;;
#         *) echo ":: unexpected VM state: $(vm_state)" >&2 ; exit 1 ;;
#     esac
#     wait_for vm_ip_is_here
#     wait_for vm_ssh_works
# }
# #}}}
# 
# stop_vm() { #{{{
#     echo ":: stopping VM"
#     s="`v domstate $shortid`"
#     case "$s" in
#         shut.off) echo ":: (already down)"; return;;
#         *) : ;;
#     esac
#     if vm_ip_is_here ; then
#         echo ":: attempting poweroff by ssh"
#         cmd=(ssh -o PreferredAuthentications=publickey -oUserKnownHostsFile=$instancekeys -i $auth_key root@$instanceip)
#         set +e
#         timeout 10 "${cmd[@]}" poweroff
#         if [ $? = 124 ] ; then
#             set -e
#             echo ":: timed out, trying hard kill"
#             v destroy $shortid
#         fi
#         set -e
#     else
#         v destroy $shortid
#     fi
#     remove_cd
# }
# #}}}
# 
# parse_script_steps() { #{{{
#     : ${script_steps:=$known_script_steps}
#     all_steps=(`echo $script_steps | tr , ' '`)
#     for step in "${all_steps[@]}" ; do
#         case "$step" in
#             install|bootstrap|compile) eval "do_step_$step=1" ;;
#             *) echo "Unknown step name $step" >& 2; exit 1;;
#         esac
#     done
# }
# #}}}
# 
# # These scripts can conceivably live outside as well.
# cado_scripts() { #{{{
#     if [ "${do_step_bootstrap}" ] ; then
#         $mydir/bootstrap-node.sh -P cado -S -nJ --platform local -n $instanceip -ak $auth_key -k ci:$auth_key.pub
#     fi
# 
#     if [ "${do_step_compile}" ] ; then
#         set -x
#         cat > $tmp/extra_nodes.sh <<EOF
# node toy-$shortid \
#     --user ci   \
#     --host $instanceip  \
#     --path /home/ci
# EOF
#         export extra_nodes=$tmp/extra_nodes.sh
#         cd $HOME/NFS/cado/cado-nfs-maintainer-scripts/
#         ./push-slaves.sh -k $auth_key toy-$shortid
#         ssh -n -F ssh/ssh-config -oPreferredAuthentications=publickey -oUserKnownHostsFile=$KNOWN_HOSTS -i $auth_key ci@$instanceip ./cado-nfs-maintainer-scripts/run.sh ./cado-nfs-maintainer-scripts/slaves/compile
#     fi
# }
# #}}}
# 
# meta_command_default() {
#     config_check
#     libvirt_check
#     dhcp_check
#     parse_script_steps
#     rc=0
# 
#     if [ "${do_step_install}" ] ; then
#         fetch_distro_image
#         if [ "$persistent" ] && [ -f "$instancedisk" ] ; then
#             previous_vm_state=$(vm_state)
#             echo ":: reusing existing $instancedisk (state of vm $shortid: $previous_vm_state)"
#         else
#             create_instance_disk
#             create_overlay_iso
#             install_vm
#             # if [ "$persistent" ] ; then
#                 remove_cd
#             # fi
#         fi
#     fi
# 
#     if [ "${do_step_bootstrap}" ] || [ "${do_step_compile}" ] ; then
#         # when the install step has just been run, we still have the kh
#         # variable set.
#         if ! [ "${KNOWN_HOSTS}" ] ; then
#             echo "Please run me with a KNOWN_HOSTS variable set" >&2
#             exit 1
#         fi
#         boot_vm
#         set +e
#         cado_scripts
#         cado_rc=$?
#         set -e
#         if ! [ "$persistent" ] || [ "$previous_vm_state" = "shut off" ] ; then
#             # remove_cd
#             stop_vm
#             if ! [ "$persistent" ] ; then
#                 echo ":: transient VM $shortid shut down"
#                 echo ":: you can resurrect it with virsh ${virsh_opts[*]} define $instancexml"
#             fi
#         fi
#     fi
# 
#     if [ "${do_step_install}" ] ; then
#         echo ":: for reference, contents of the known_hosts file for this machine [$shortid, $instanceip]"
#         cat "$KNOWN_HOSTS"
#     fi
# 
#     exit $cado_rc
# }
# 
# meta_command_status() {
#     config_check
#     libvirt_check
#     set +e
#     echo ":: VM $shortid has status: $(vm_state)"
#     exit 0
# }
# 
# meta_command_up() {
#     config_check
#     libvirt_check
#     boot_vm
#     exit 0
# }
# 
# meta_command_nuke() {
#     # config_check
#     libvirt_check
#     echo ":: destroying VM"
#     set +e
#     v destroy $shortid >/dev/null 2>&1
#     v undefine $shortid >/dev/null 2>&1
#     ssh root@localhost rm -f $instancedisk $instancekeys $instancexml >/dev/null 2>&1
#     echo ":: VM is gone, bye"
#     exit 0
# }
# 
# meta_command_ssh() {
#     config_check
#     libvirt_check
#     echo ":: connecting to VM $shortid"
#     if ! [ -f "$instancekeys" ] ; then
#         echo ":: no file $instancekeys -- are you sure the VM hasn't been killed ?" >&2
#         exit 1
#     fi
#     cmd=(ssh -o PreferredAuthentications=publickey -oUserKnownHostsFile=$instancekeys -i $auth_key ${ssh_login=root}@$instanceip)
#     echo ":: ${cmd[*]}"
#     exec "${cmd[@]}"
# }
# 
# case "$meta_command" in
#     ssh) meta_command_ssh ;;
#     status) meta_command_status ;;
#     up) meta_command_up ;;
#     nuke) meta_command_nuke ;;
#     default) meta_command_default ;;
#     *) meta_command_default ;;
# esac
