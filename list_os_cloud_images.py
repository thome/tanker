#!/usr/bin/env python3
import gi
import sys
import re
gi.require_version("Libosinfo", "1.0")
from gi.repository import Libosinfo
loader=Libosinfo.Loader()
loader.process_default_path()
db=loader.get_db()
oss=db.get_os_list()

for os in oss.get_elements():
    for i in os.get_image_list().get_elements():
        short = os.get_short_id()
        tag = re.sub("^" + os.get_distro(), "", short)
        L = [os.get_distro(),tag,i.get_architecture(),i.get_format(),i.get_url()]
        keep = True
        if len(sys.argv) >= 2:
            askdist,asktag = (sys.argv[1].split(":")+[None])[:2]
            if L[0] != askdist or (asktag != None and L[1] != asktag):
                keep = False

        if not keep:
            continue

        for i in range(2,len(sys.argv)):
            if L[i] != sys.argv[i]:
                keep = False

        if keep:
            print(" ".join(L))

