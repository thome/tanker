#!/bin/bash

database="$1"
version="$2"

pkg_install() {
    base=(env ASSUME_ALWAYS_YES=yes pkg install gmp)
    message="NOTE: The network failure messages above are harmless"
    full=()
    full+=("${base[@]}")
    full+=(\|\| \(route get 8.8.8.8 \|\| echo $message \; service dhclient restart em0 \;)
    full+=("${base[@]}")
    full+=(\))
    echo "${full[@]}"
}

./tanker.sh -B "$database" vm build -R -t "my-freebsd:$version" "freebsd:$version" @guest `pkg_install gmake`
./tanker.sh -B "$database" vm ssh --shutdown -u user "my-freebsd:$version" uname -a
./tanker.sh -B "$database" vm run --rm "my-freebsd:$version" @guest `pkg_install gmp`
