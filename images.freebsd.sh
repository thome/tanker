#!/bin/sh


fetch_images() {
    for major in 10 11 12 13 14 ; do
        for minor in 0 1 2 3 4 5 ; do
            URL="http://ftp.freebsd.org/pub/FreeBSD/releases/ISO-IMAGES/$major.$minor/FreeBSD-$major.$minor-RELEASE-$FREEBSD_ARCH-bootonly.iso"
            if curl -sI "$URL" | head -n 1 | grep -qw '200' ; then
                query_info=
                if [ "$FREEBSD_ARCH" != amd64 ] ; then
                    query_info="?arch=$FREEBSD_ARCH"
                fi
                echo "freebsd $major.$minor $ARCH qcow2 freebsd-cloud-image:///$major.$minor$query_info"
            fi
        done
    done
}

ARCH=x86_64 ; FREEBSD_ARCH=amd64 ; fetch_images
ARCH=i386 ; FREEBSD_ARCH=i386 ; fetch_images
